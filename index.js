console.log("Pokemon");

	let trainer = {
		name:"Ethaneya Villacampa",
		age : 10,
		pokemon:["pikachu", "bulbasaur", "squirtle", "charmander"],
		talk : function(){
			console.log("Pikachu! I choose you!");
		}, 
		friends:{
			hoenn:["May", "Max"],
			kanto:["Brock", "Misty"]
		}
	} 

	console.log(trainer);
	//dot notation
	console.log("Result of dot notation");
	console.log(trainer.name);
	//bracket notation
	console.log("Result of square bracket notation");
	console.log(trainer["pokemon"]);
	// talk method
	console.log("Result of talkmethod");
	trainer.talk();

	function pokemon(name,level){
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = level*2;
		this.pokemonAttack = level;

		this.tackle = function(pokemonTarget){
			console.log(this.pokemonName+" attacked "+pokemonTarget.pokemonName);
			let healthPercent = pokemonTarget.pokemonHealth - this.pokemonAttack;
			console.log(pokemonTarget.pokemonName+"'s health is now reduced to "+healthPercent);

			if(healthPercent<=0){
				pokemonTarget.faint(pokemon);
			}
		}
		this.faint = function(){
				console.log(this.pokemonName+" has fainted!")
			}
	}
	let balbasaur = new pokemon("balbasaur", 56);
	console.log(balbasaur)

	let pikachu = new pokemon("Pikachu", 100);
	console.log(pikachu);

	let gyarados = new pokemon("gyarados", 200);
	console.log(gyarados);

	balbasaur.tackle(pikachu);







